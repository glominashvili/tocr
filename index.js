'use strict';

const Hapi = require('hapi');
const fs = require('fs');
const server = new Hapi.Server();
var exec = require('child_process').exec;
var cmd = 'tesseract %img% %text% -l eng';

var uuid = require('uuid');
server.connection({ 
    host: 'localhost', 
    port: 8000 
});

const util = require('util')




server.register(require('inert'), (err) => {

    if (err) {
        throw err;
    }

    server.route({
        method: 'POST',
        path: '/reader',
        config: {
            cors: {
                origin: ['*'],
                additionalHeaders: ['cache-control', 'x-requested-with']
            },
            payload: {
                output: 'stream',
                maxBytes: 10485760,
                parse: true,
                allow: 'multipart/form-data'
            },

            handler: function (request, reply) {
                var data = request.payload;
                console.log(data);
                if (data.image && data.image != 'null') {
                    var name = uuid.v4() + "-" + data.image.hapi.filename.replace(" ", "");
                    var path = __dirname + "/uploads/" + name;
                    var file = fs.createWriteStream(path);

                    file.on('error', function (err) { 
                        reply(JSON.stringify({
                            error: "No Image File",
                            message: "No File Received"
                        })).code(500);
                    });

                    data.image.pipe(file);

                    data.image.on('end', function (err) { 
                        var command = cmd.replace('%img%', path).replace('%text%', (path));
                        exec(command, function(error, stdout, stderr) {
                            if(error){
                                reply(JSON.stringify({
                                    error: "System Error",
                                    message: "Can't Perform Image Read Operation"
                                })).code(500);
                            }else{
                                fs.readFile(path+".txt", 'utf8', function (terr, tdata) {
                                    if (terr) {
                                        reply(JSON.stringify({
                                            error: "System Error",
                                            message: "Can't Perform Read Operation"
                                        })).code(500);
                                    }else{
                                        var obj = {"data": tdata};
                                        reply(JSON.stringify(obj)).code(200);
                                    }
                                });
                            }
                        });
                    });
                } else {
                    reply(JSON.stringify({
                        error: "No Image File",
                        message: "No File Received"
                    })).code(400);
                }

            }
        }
    });

    server.route({
        method: 'GET',
        path: '/',
        handler: function (request, reply) {
            reply.file('./index.html');
        }
    });

    server.route({
        method: 'GET',
        path: '/index.html',
        handler: function (request, reply) {
            reply.file('./index.html');
        }
    });

    server.start((err) => {

        if (err) {
            throw err;
        }

        console.log('Server running at:', server.info.uri);
    });
});

